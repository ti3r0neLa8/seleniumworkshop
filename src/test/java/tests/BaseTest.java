package tests;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.LoginPage;
import pages.SetupPage;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class BaseTest {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected Actions actions;
    protected JavascriptExecutor js;

    String environmentLocation = "src/test/resources/environment.json";
    String credentialsLocation = "src/test/resources/credentials.json";
    String mode; //1,2,3,headless
    String chromedriverLocation;

    String URL;
    String username;
    String password;

    @BeforeEach
    public void setup() throws IOException, ParseException {

        getEnvironment();
        getCredentials();

        System.setProperty("webdriver.chrome.driver", chromedriverLocation);

        // Block notifications pop-up
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", prefs);
        //options.setPageLoadStrategy(PageLoadStrategy.EAGER);

        // Headless setup
        switch (mode){
            case "headless": options.addArguments("--headless", "--no-sandbox", "--disable-dev-shm-usage", "--disable-gpu", "--window-size=1920,1080","--ignore-certificate-errors");
                break;
            default: options.addArguments("--window-size=1920,1040","--ignore-certificate-errors");
                break;
        }

        driver = new ChromeDriver(options);
        actions = new Actions(driver);
        wait = new WebDriverWait(driver, 30, 100);
        js = ((JavascriptExecutor)driver);

        //Window size and position
        switch (mode) {
            case("1"): driver.manage().window().setPosition(new Point(0, -1080));
                //driver.manage().window().fullscreen();
                break;
            case("2"): driver.manage().window().setPosition(new Point(-1920, -1070));
                //driver.manage().window().fullscreen();
                break;
            case("3"): driver.manage().window().setPosition(new Point(-980, -1080));
                //driver.manage().window().fullscreen();
                break;
            default:
                break;
        }

        LoginPage loginPage = new LoginPage(driver);
        loginPage.loginToSalesforce(URL, username, password).waitForCarouselOnHomePage().clickAppLauncherIcon().selectEnxooCommerceAdminApp();

    }

    //@AfterEach
    public void tearDown() {
        driver.quit();
    }

    public void getEnvironment() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object object = parser.parse(new FileReader(environmentLocation));
        JSONObject jsonObject = (JSONObject) object;
        mode = (String) jsonObject.get("mode");
        chromedriverLocation = (String) jsonObject.get("chromedriverLocation");
    }

    public void getCredentials() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object object = parser.parse(new FileReader(credentialsLocation));
        JSONObject jsonObject = (JSONObject) object;
        URL = (String) jsonObject.get("URL");
        username = (String) jsonObject.get("username");
        password = (String) jsonObject.get("password");
    }
}
