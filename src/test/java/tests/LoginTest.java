package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pages.LoginPage;

public class LoginTest extends BaseTest {

    @Test
    public void shouldCorrectLogin() {

        String USERNAME = ""; //input test username
        String PASSWORD = ""; //input test password
        String loggedUser = "User User";


        LoginPage loginPage = new LoginPage(driver);

        String actualLoggedUser = loginPage.testLoginToSalesforce(USERNAME, PASSWORD).getLoggedUsername();

        Assertions.assertEquals(loggedUser, actualLoggedUser, "Wrong username!!");
    }

    @Test
    public void shouldDisplayErrorForEmptyPassword() {

        String USERNAME = "test-g1iqtlmlu2gj@example.com";
        String PASSWORD = "";

        LoginPage loginPage = new LoginPage(driver);

        loginPage.testLoginToSalesforce(USERNAME, PASSWORD);

        String errorMessage = loginPage.getErrorMessage();

        Assertions.assertEquals("Please enter your password.", errorMessage, "Wrong message!!");
    }

    @Test
    public void shouldDisplayErrorForIncorrectPassword() {

        String USERNAME = "test-g1iqtlmlu2gj@example.com";
        String PASSWORD = "%sW9j2Q@0^x";

        LoginPage loginPage = new LoginPage(driver);

        loginPage.testLoginToSalesforce(USERNAME, PASSWORD);

        String errorMessage = loginPage.getErrorMessage();

        Assertions.assertEquals("Please check your username and password. If you still can't log in, contact your Salesforce administrator.", errorMessage, "Wrong message!!");
    }

    @Test
    public void shouldNotLoginWithEmptyCredentials() {

        String USERNAME = "";
        String PASSWORD = "";

        LoginPage loginPage = new LoginPage(driver);

        loginPage.testLoginToSalesforce(USERNAME, PASSWORD);

        String actualUrl = driver.getCurrentUrl();

        Assertions.assertEquals("https://test.salesforce.com/", actualUrl);
    }
}
