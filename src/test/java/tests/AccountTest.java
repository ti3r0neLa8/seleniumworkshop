package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import pages.AccountDetailsPage;
import pages.EnxCommerceAdminGlobalHeaderPage;
import pages.NewAccountPage;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AccountTest extends BaseTest {

    @Test
    public void shouldCorrectlyCreateAccount(){

        String accountName = "TestAccount3";
        String accountRating = null;

        NewAccountPage newAccountPage = new NewAccountPage(driver);
        EnxCommerceAdminGlobalHeaderPage headerPage = new EnxCommerceAdminGlobalHeaderPage(driver);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver);

        headerPage.openAccountsTab().clickNewButton().inputAccountName(accountName).inputAccountRating(accountRating).clickSaveButton();

        String actualAccountName = accountDetailsPage.getAccountName();
        String actualAccountRating = accountDetailsPage.getAccountRating();

        Assertions.assertEquals(accountName, actualAccountName, "Wrong account name, expected result: " + accountName);
        Assertions.assertEquals(accountRating, actualAccountRating, "Wrong rating");
    }
}
