package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class NewAccountPage extends BasePage {
    public NewAccountPage(WebDriver driver) {
        super(driver);
    }

    By inputNameFieldSelector = By.xpath(".//label/span[text()='Account Name']/../following-sibling::input");
    By saveButtonSelector = By.xpath(".//button[@title='Save']");

    public NewAccountPage inputAccountName(String accountName){
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(inputNameFieldSelector, 0));
        driver.findElement(inputNameFieldSelector).sendKeys(accountName);
        return new NewAccountPage(driver);
    }

    public NewAccountPage inputAccountRating(String rating){
        return new NewAccountPage(driver);
    }

    public AccountDetailsPage clickSaveButton(){
        driver.findElement(saveButtonSelector).click();
        return new AccountDetailsPage(driver);
    }
}
