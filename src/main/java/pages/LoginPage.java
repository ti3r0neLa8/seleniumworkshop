package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    private By usernameFieldSelector = By.cssSelector("input#username");
    private By passwordFieldSelector = By.cssSelector("input#password");
    private By loginButtonSelector = By.cssSelector("input#Login");
    private By errorMessageContainerSelector = By.cssSelector("div#error");

    public SetupPage testLoginToSalesforce(String login, String password) {
        driver.manage().deleteAllCookies();
        driver.get("https://test.salesforce.com");
        wait.until(ExpectedConditions.visibilityOfElementLocated(usernameFieldSelector));
        driver.findElement(usernameFieldSelector).sendKeys(login);
        driver.findElement(passwordFieldSelector).sendKeys(password);
        driver.findElement(loginButtonSelector).click();
        return new SetupPage(driver);
    }

    public SetupPage loginToSalesforce(String url, String login, String password) {
        driver.get(url);
        wait.until(ExpectedConditions.visibilityOfElementLocated(usernameFieldSelector));
        driver.findElement(usernameFieldSelector).sendKeys(login);
        driver.findElement(passwordFieldSelector).sendKeys(password);
        driver.findElement(loginButtonSelector).click();
        return new SetupPage(driver);
    }

    public String getErrorMessage(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(errorMessageContainerSelector));
        return driver.findElement(errorMessageContainerSelector).getText();
    }
}
