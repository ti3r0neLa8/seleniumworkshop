package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class EnxCommerceAdminGlobalHeaderPage extends BasePage {
    public EnxCommerceAdminGlobalHeaderPage(WebDriver driver) {
        super(driver);
    }

    private By accountsTabSelector = By.xpath(".//a[@title='Accounts']");

    public AccountsListPage openAccountsTab(){
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(accountsTabSelector, 0));
        WebElement accountsTab = driver.findElement(accountsTabSelector);
        actions.moveToElement(accountsTab).click().build().perform();
    return new AccountsListPage(driver);
    }
}
