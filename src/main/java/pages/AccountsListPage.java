package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AccountsListPage extends BasePage {
    public AccountsListPage(WebDriver driver) {
        super(driver);
    }

    private By newButtonSelector = By.cssSelector("a[title='New']");

    public NewAccountPage clickNewButton(){
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(newButtonSelector, 0));
        driver.findElement(newButtonSelector).click();
        return new NewAccountPage(driver);
    }
}
