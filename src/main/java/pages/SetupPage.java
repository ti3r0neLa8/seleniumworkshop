package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SetupPage extends BasePage {
    public SetupPage(WebDriver driver) {
        super(driver);
    }

    private By uiImageSelector = By.cssSelector("div[class='tooltipTrigger tooltip-trigger uiTooltip'] span[class='uiImage']");
    private By loggedUsernameSelector = By.cssSelector("a[class='profile-link-label']");
    private By carouselContainerSelector = By.cssSelector("div[class='carousel']");
    private By appLauncherIconSelector = By.cssSelector("div[class='slds-icon-waffle']");
    private By searchAppFieldDivSelctor = By.xpath(".//input[@class='slds-input']/..");
    private By searchAppFieldSelctor = By.xpath(".//input[@class='slds-input']");
    private By searchAppFieldSelctorCSS = By.cssSelector("input[placeholder='Search apps and items...']");
    private By allAppButton = By.xpath(".//button[text()='View All']");

    private String selecteAppSelector = ".//b[text()='<appName>']/..";

    public SetupPage waitForCarouselOnHomePage(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(carouselContainerSelector));
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new SetupPage(driver);
    }

    public String getLoggedUsername() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(uiImageSelector));
        driver.findElement(uiImageSelector).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(loggedUsernameSelector));
        return driver.findElement(loggedUsernameSelector).getText();
    }

    public SetupPage clickAppLauncherIcon(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(appLauncherIconSelector));
        WebElement applauncherIcon = driver.findElement(appLauncherIconSelector);
        actions.moveToElement(applauncherIcon).click().build().perform();
        WebDriverWait wait5 = new WebDriverWait(driver, 5, 100);
        try{wait5.until(ExpectedConditions.numberOfElementsToBe(By.xpath(".//lightning-avatar"), 7));}
        catch (Exception e) {
            actions.moveToElement(applauncherIcon).click().pause(1000).click().build().perform();
        }
        return new SetupPage(driver);
    }

    public EnxooCommerceAdminPage selectEnxooCommerceAdminApp(){

        String appName = "Enxoo Commerce Admin";

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(allAppButton, 0));
        driver.findElement(searchAppFieldSelctor).sendKeys(appName);
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchAppFieldSelctor));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selecteAppSelector.replace("<appName>", appName))));
        driver.findElement(By.xpath(selecteAppSelector.replace("<appName>", appName))).click();
        return new EnxooCommerceAdminPage(driver);
    }
}
