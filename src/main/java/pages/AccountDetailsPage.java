package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AccountDetailsPage extends BasePage {
    public AccountDetailsPage(WebDriver driver) {
        super(driver);
    }

    By accountNameSelector = By.xpath(".//span[@class='custom-truncate uiOutputText']");

    public String getAccountName(){
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(accountNameSelector, 0));
        return driver.findElement(accountNameSelector).getText();
    }

    public String getAccountRating(){
        return null;
    }
}
