package helpers;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

/************************************************************************
 Description : Class used for checking test execution status (pass/fail)
 Created by : Tomasz Zulawnik (tomasz.zulawnik@gmail.com)

 Class History
 -------------------------------------------------------------------------
 Date 		Author		 							    Reason
 2020-02-11	Tomasz Zulawnik (tomasz.zulawnik@gmail.com)	Class created

 ************************************************************************/

public class TestStatus implements AfterTestExecutionCallback {
    public boolean isFailed;
    @Override
    public void afterTestExecution(ExtensionContext extensionContext) throws Exception {
        if(extensionContext.getExecutionException().isPresent()){
            isFailed = true;
        }
        else isFailed = false;
    }
}
